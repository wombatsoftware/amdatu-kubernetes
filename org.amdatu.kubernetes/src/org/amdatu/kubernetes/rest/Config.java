/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes.rest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.osgi.service.cm.ConfigurationException;

import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.AsyncHttpClientConfig.Builder;
import com.ning.http.client.Realm;
import com.ning.http.client.Realm.AuthScheme;

/**
 * Provides the configuration for the {@link KubernetesRestClient}.
 */
class Config {
    private static final String KEY_URL = "kubernetesurl";
    private static final String KEY_API_VERSION = "apiversion";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_CLIENT_CA = "clientCA.file";
    private static final String KEY_CLIENT_CA_TYPE = "clientCA.type";
    private static final String KEY_CLIENT_CA_PASSWORD = "clientCA.password";
    private static final String KEY_CLIENT_CERT = "clientCert.file";
    private static final String KEY_CLIENT_CERT_TYPE = "clientCert.type";
    private static final String KEY_CLIENT_CERT_PASSWORD = "clientCert.password";
    private static final String KEY_CLIENT_CERT_KEY_PASSWORD = "clientCert.keyPassword";
    private static final String KEY_ACCEPT_ANY_CERT = "acceptAnyCert";

    private static final String DEFAULT_API_VERSION = "v1";

    private final URI m_kubernetesUrl;
    private final String m_apiVersion;
    private final Map<String, String> m_properties;

    public Config(Dictionary<String, ?> properties) throws ConfigurationException {
        URI kubernetesUrl = null;
        String apiVersion = DEFAULT_API_VERSION;
        Map<String, String> props = new HashMap<>();

        if (properties != null) {
            Object val = properties.get(KEY_URL);
            if (val == null || !(val instanceof String)) {
                throw new ConfigurationException(KEY_URL, "missing or invalid value!");
            }
            try {
                kubernetesUrl = new URI((String) val);
            }
            catch (URISyntaxException e) {
                throw new ConfigurationException(KEY_URL, "invalid value: not an URI!", e);
            }

            val = properties.get(KEY_API_VERSION);
            if (val != null && !(val instanceof String)) {
                throw new ConfigurationException(KEY_API_VERSION, "invalid value, must be a string!");
            }
            if (val != null) {
                apiVersion = (String) val;
            }

            Collections.list(properties.keys()).stream()
                .filter(k -> !KEY_URL.equals(k) && !KEY_API_VERSION.equals(k))
                .forEach(k -> props.put(k, toString(properties.get(k))));
        }

        m_kubernetesUrl = kubernetesUrl;
        m_apiVersion = apiVersion;
        m_properties = props;
    }

    /**
     * Creates a new {@link Config} instance.
     */
    public Config(String kubernetesUrl, String apiVersion) {
        m_kubernetesUrl = URI.create(kubernetesUrl);
        m_apiVersion = apiVersion;
        m_properties = Collections.emptyMap();
    }

    private static String toString(Object o) {
        if (o == null) {
            return "";
        }
        return ((String) o).trim();
    }

    public AsyncHttpClientConfig createHttpClientConfig() {
        Builder builder = new AsyncHttpClientConfig.Builder()
            .setAllowPoolingConnections(true)
            .setPooledConnectionIdleTimeout(60000)
            .setReadTimeout(10000)
            .setConnectTimeout(10000)
            .setRequestTimeout(10000)
            .setWebSocketTimeout(10000);
        // In case you really don't care about security...
        builder.setAcceptAnyCertificate(Boolean.parseBoolean(m_properties.get(KEY_ACCEPT_ANY_CERT)));

        if (m_properties.containsKey(KEY_CLIENT_CA) || m_properties.containsKey(KEY_CLIENT_CERT)) {
            try {
                builder.setSSLContext(createSSLContext());
            }
            catch (IOException | ConfigurationException e) {
                throw new RuntimeException("Creating HTTP client configuration failed!", e);
            }
        }

        if (m_properties.containsKey(KEY_USERNAME)) {
            builder.setRealm(new Realm.RealmBuilder()
                .setPrincipal(m_properties.get(KEY_USERNAME))
                .setPassword(m_properties.get(KEY_PASSWORD))
                .setScheme(AuthScheme.BASIC)
                .setUsePreemptiveAuth(true)
                .build());
        }

        return builder.build();
    }

    /**
     * @return the version of the Kubernetes API to use, cannot be <code>null</code>.
     */
    public String getApiVersion() {
        return m_apiVersion;
    }

    /**
     * @return the URL to the Kubernetes API server, cannot be <code>null</code>.
     */
    public URI getKubernetesURL() {
        return m_kubernetesUrl;
    }

    private KeyManager[] createKeyManagers() throws IOException, ConfigurationException {
        String clientCert = m_properties.get(KEY_CLIENT_CERT);
        if (clientCert == null || "".equals(clientCert)) {
            return null;
        }
        String type = m_properties.get(KEY_CLIENT_CERT_TYPE);
        if (type == null || "".equals(type)) {
            type = KeyStore.getDefaultType();
        }
        String pwd = m_properties.get(KEY_CLIENT_CERT_PASSWORD);
        char[] ksPwd;
        if (pwd == null) {
            ksPwd = new char[0];
        }
        else {
            ksPwd = pwd.toCharArray();
        }
        pwd = m_properties.get(KEY_CLIENT_CERT_KEY_PASSWORD);
        char[] keyPwd;
        if (pwd == null) {
            keyPwd = new char[0];
        }
        else {
            keyPwd = pwd.toCharArray();
        }

        try {
            KeyStore ks = KeyStore.getInstance(type);
            try (InputStream is = Files.newInputStream(new File(clientCert).toPath())) {
                ks.load(is, ksPwd);
            }

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, keyPwd);
            return kmf.getKeyManagers();
        }
        catch (KeyStoreException e) {
            throw new ConfigurationException(KEY_CLIENT_CA_TYPE, "Invalid or unsupported truststore type: " + type, e);
        }
        catch (GeneralSecurityException e) {
            throw new ConfigurationException(KEY_CLIENT_CA, "Invalid or unsupported truststore!" + type, e);
        }
    }

    private TrustManager[] createTrustManagers() throws IOException, ConfigurationException {
        String clientCA = m_properties.get(KEY_CLIENT_CA);
        if (clientCA == null || "".equals(clientCA)) {
            return null;
        }
        String type = m_properties.get(KEY_CLIENT_CA_TYPE);
        if (type == null || "".equals(type)) {
            type = KeyStore.getDefaultType();
        }
        String pwdStr = m_properties.get(KEY_CLIENT_CA_PASSWORD);
        char[] pwd;
        if (pwdStr == null) {
            pwd = new char[0];
        }
        else {
            pwd = pwdStr.toCharArray();
        }

        try {
            KeyStore ks = KeyStore.getInstance(type);
            try (InputStream is = Files.newInputStream(new File(clientCA).toPath())) {
                ks.load(is, pwd);
            }

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ks);
            return tmf.getTrustManagers();
        }
        catch (KeyStoreException e) {
            throw new ConfigurationException(KEY_CLIENT_CA_TYPE, "Invalid or unsupported truststore type: " + type, e);
        }
        catch (GeneralSecurityException e) {
            throw new ConfigurationException(KEY_CLIENT_CA, "Invalid or unsupported truststore!" + type, e);
        }
    }

    private SSLContext createSSLContext() throws IOException, ConfigurationException {
        try {
            TrustManager[] tms = createTrustManagers();
            KeyManager[] kms = createKeyManagers();

            SSLContext sslCtx = SSLContext.getInstance("TLS");
            sslCtx.init(kms, tms, null);
            return sslCtx;
        }
        catch (KeyManagementException e) {
            throw new ConfigurationException(KEY_CLIENT_CERT, "Invalid client certificate!", e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("JVM does not support TLS or strong PRNGs?", e);
        }
    }
}
