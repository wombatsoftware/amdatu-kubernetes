/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes.rest;

import java.util.Dictionary;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import org.amdatu.kubernetes.Kubernetes;
import org.amdatu.kubernetes.WatchUpdate;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ConfigurationDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.osgi.service.cm.ConfigurationException;

import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceList;
import io.fabric8.kubernetes.api.model.NodeList;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.api.model.ReplicationControllerList;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.api.model.SecretList;
import rx.Observable;
import rx.Subscriber;

@Component
public class KubernetesRestClient implements Kubernetes {
    private volatile Config m_config;
    // Managed in start/stop
    private volatile HttpClient m_httpClient;

    public KubernetesRestClient() {
        // Used by Felix DM...
    }

    public KubernetesRestClient(String kubernetesUrl, String apiVersion) {
        m_config = new Config(kubernetesUrl, apiVersion);
        m_httpClient = new HttpClient(m_config);
    }

    private static void sleep(long msecs) {
        try {
            TimeUnit.MILLISECONDS.sleep(msecs);
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public Observable<Namespace> createNamespace(Namespace ns) {
        String url = createUrlBuilder().path("namespaces").build();

        return Observable.create(observer -> m_httpClient.postObject(url, ns, observer));
    }

    @Override
    public Observable<ReplicationController> createReplicationController(String namespace, ReplicationController rc) {
        String url = createUrlBuilder().namespace(namespace).path("replicationcontrollers").build();

        return Observable.create(observer -> m_httpClient.postObject(url, rc, observer));
    }

    @Override
    public Observable<Boolean> deleteNamespace(String namespace) {
        return Observable.concat(doDeleteNamespace(namespace), doTestNamespaceIsGone(namespace))
        		.reduce((o1, o2) -> o1.booleanValue() && o2.booleanValue());
    }

    @Override
    public Observable<Pod> getPod(String namespace, String name) {
        String url = createUrlBuilder().namespace(namespace).path("pods").path(name).build();

        return Observable.create(observer -> m_httpClient.getObject(url, Pod.class, observer));
    }
    
    @Override
   	public Observable<Boolean> deletePod(String namespace, String name) {
    	return deleteResource(namespace, "pods", name);
    }

    @Override
    public Observable<ReplicationController> getReplicationController(String namespace, String name) {
        String url = createUrlBuilder().namespace(namespace).path("replicationcontrollers").path(name).build();

        return Observable.create(observer -> m_httpClient.getObject(url, ReplicationController.class, observer));
    }
    
    @Override
	public Observable<ReplicationControllerList> listReplicationControllers(String namespace) {
    	String url = createUrlBuilder().namespace(namespace).path("replicationcontrollers").build();

        return Observable.create(observer -> m_httpClient.getObject(url, ReplicationControllerList.class, observer));
	}

    @Override
    public Observable<NamespaceList> listNamespaces() {
        String url = createUrlBuilder().path("namespaces").build();

        return Observable.create(observer -> m_httpClient.getObject(url, NamespaceList.class, observer));
    }

    @Override
    public Observable<NodeList> listNodes() {
        String url = createUrlBuilder().path("nodes").build();

        return Observable.create(observer -> m_httpClient.getObject(url, NodeList.class, observer));
    }

    @Override
    public Observable<PodList> listPods(String namespace, Map<String, String> labelSelector) {
        String url = createUrlBuilder().namespace(namespace).path("pods").labels(labelSelector).build();

        return Observable.create(observer -> m_httpClient.getObject(url, PodList.class, observer));
    }
    
    @Override
    public Observable<ReplicationController> scale(String namespace, String rcName, int nrOfReplicas) {
        return Observable.create(subscriber -> {
            getReplicationController(namespace, rcName).subscribe(rc -> {
                rc.getSpec().setReplicas(nrOfReplicas);

                updateReplicationController(namespace, rc).subscribe(updated -> {
                    subscriber.onNext(updated);
                    subscriber.onCompleted();
                } ,
                    t -> subscriber.onError(t));
            } ,
                t -> subscriber.onError(t));
        });
    }
    
    /**
     * Managed by Felix DM.
     */
    @ConfigurationDependency(pid = "org.amdatu.kubernetes")
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        m_config = new Config(properties);
        m_httpClient = new HttpClient(m_config);
    }

    @Override
    public Observable<ReplicationController> updateReplicationController(String namespace, ReplicationController rc) {
        String url = createUrlBuilder().namespace(namespace).path("replicationcontrollers").path(rc.getMetadata().getName()).build();

        rc.getMetadata().setResourceVersion(null);
        rc.setStatus(null);

        return Observable.create(observer -> m_httpClient.putObject(url, rc, observer));
    }

    @Override
    public Observable<PodList> waitForPods(String namespace, Map<String, String> labelSelector, int expected, Predicate<Pod> filter) {
        AtomicInteger backupTime = new AtomicInteger(100);

        return Observable.<PodList> create(subscriber -> {
            listPods(namespace, labelSelector).subscribe(new Subscriber<PodList>() {
                @Override
                public void onCompleted() {
                    subscriber.onCompleted();
                }

                @Override
                public void onError(Throwable t) {
                    subscriber.onError(t);
                }

                @Override
                public void onNext(PodList list) {
                    if (list.getItems().stream().filter(filter).count() == expected) {
                        subscriber.onNext(list);
                        subscriber.onCompleted();
                    }
                    else {
                        sleep(backupTime.getAndUpdate(v -> v * 2));
                        subscriber.onError(new RuntimeException("Not enough pods found"));
                    }
                }
            });
        }).retry(10);
    }

    @Override
    public Observable<WatchUpdate<Pod>> watchPods(String namespace, Map<String, String> labelSelector) {
        String url = createUrlBuilder().namespace(namespace).path("pods").buildWs();

        return Observable.<WatchUpdate<Pod>> create(subscriber -> m_httpClient.watchObject(url, Pod.class, subscriber));
    }

	@Override
	public Observable<SecretList> listSecrets(String namespace) {
        String url = createUrlBuilder().namespace(namespace).path("secrets").build();

        return Observable.create(observer -> m_httpClient.getObject(url, SecretList.class, observer));
	}

	@Override
	public Observable<Secret> getSecret(String namespace, String name) {
        String url = createUrlBuilder().namespace(namespace).path("secrets").path(name).build();

        return Observable.create(observer -> m_httpClient.getObject(url, Secret.class, observer));
	}

	@Override
	public Observable<Secret> setSecret(String namespace, String name, Map<String, String> data) {
		String url;
		Secret secret = getSecret(namespace, name).toBlocking().single();
		if (secret == null) {
			// create new Secret
			ObjectMeta metadata = new ObjectMeta();
			metadata.setName(name);
			Secret newSecret = new Secret(
					io.fabric8.kubernetes.api.model.Secret.ApiVersion.fromValue(m_config.getApiVersion()),
					data, "Secret", metadata, "Opaque");
			url = createUrlBuilder().namespace(namespace).path("secrets").build();
	        return Observable.create(observer -> m_httpClient.postObject(url, newSecret, observer));
		} else {
			// update existing Secret
			secret.setData(data);
			url = createUrlBuilder().namespace(namespace).path("secrets").path(name).build();
	        return Observable.create(observer -> m_httpClient.putObject(url, secret, observer));
		}
	}
	
	@Override
	public Observable<Secret> setSecret(String namespace, Secret secret) {
		String name = secret.getMetadata().getName();
		return getSecret(namespace, name).
				flatMap(oldSecret -> setSecret(namespace, secret, oldSecret));

	}

	private Observable<Secret> setSecret(String namespace, Secret newSecret, Secret oldSecret) {
		String name = newSecret.getMetadata().getName();
		if (oldSecret == null) {
			// new Secret
			String url = createUrlBuilder().namespace(namespace).path("secrets").build();
	        return Observable.create(observer -> m_httpClient.postObject(url, newSecret, observer));
		} else {
			// update existing Secret
			oldSecret.setData(newSecret.getData());
			String url = createUrlBuilder().namespace(namespace).path("secrets").path(name).build();
	        return Observable.create(observer -> m_httpClient.putObject(url, oldSecret, observer));
		}
	}
	
	@Override
	public Observable<Boolean> deleteSecret(String namespace, String name) {
		return deleteResource(namespace, "secrets", name);
	}

	@Override
	public Observable<String> getLogs(String namespace, String name) {
		String url = createUrlBuilder().namespace(namespace).path("pods").path(name).path("log").build();
        return Observable.create(observer -> m_httpClient.getString(url, observer));
	}
	
	@Override
	public Observable<String> getLogs(String namespace, String name, int tail) {
		String url = createUrlBuilder().namespace(namespace).path("pods").path(name).path("log").build() + "?tailLines=" + tail;;
        return Observable.create(observer -> m_httpClient.getString(url, observer));
	}
	
	@Override
	public Observable<String> getLogsLimited(String namespace, String name, long limitBytes) {
		String url = createUrlBuilder().namespace(namespace).path("pods").path(name).path("log").build() + "?limitBytes=" + limitBytes;
        return Observable.create(observer -> m_httpClient.getString(url, observer));
	}
	
    /**
     * Managed by Felix DM and called when this component is started.
     */
    @Start
    protected final void start() {
        // Nop
    }

    /**
     * Managed by Felix DM and called when this component is stopped.
     */
    @Stop
    protected final void stop() {
        if (m_httpClient != null) {
            m_httpClient.close();
            m_httpClient = null;
        }
    }

    private Observable<Boolean> doDeleteNamespace(String namespace) {
        String url = createUrlBuilder().namespace(namespace).build();

        return Observable.<Integer> create(observer -> m_httpClient.deleteObject(url, observer))
            .map(f -> f.intValue() == 200);
    }

    private Observable<Boolean> doTestNamespaceIsGone(String namespace) {
        AtomicInteger backoffTime = new AtomicInteger(500);
        String url = createUrlBuilder().namespace(namespace).build();

        return Observable.<Boolean> create(observer -> {
            m_httpClient.getStatus(url, new Subscriber<Integer>() {
                @Override
                public void onCompleted() {
                    observer.onCompleted();
                }

                @Override
                public void onError(Throwable t) {
                    observer.onError(t);
                }

                @Override
                public void onNext(Integer val) {
                    if (val == 404) {
                        observer.onNext(Boolean.TRUE);
                        observer.onCompleted();
                    }
                    else {
                        sleep(backoffTime.getAndUpdate(v -> v * 2));
                        observer.onError(new RuntimeException());
                    }
                }
            });
        }).retry(10);
    }

    private Observable<Boolean> deleteResource(String namespace, String resourceType, String name) {
        Observable<Boolean> deleteObservable = doDeleteResource(namespace, resourceType, name);
        Observable<Boolean> testObservable = doTestResourceIsGone(namespace, resourceType, name);
        
        return Observable.concat(deleteObservable, testObservable)
        		.reduce((o1, o2) -> o1.booleanValue() && o2.booleanValue());
    }
    
    private Observable<Boolean> doDeleteResource(String namespace, String resourceType, String name) {
        String url = createUrlBuilder().namespace(namespace).path(resourceType).path(name).build();

        return Observable.<Integer> create(observer -> m_httpClient.deleteObject(url, observer)).doOnNext(n -> {
        	System.out.println(url);
         	System.out.println("Next:");
        	System.out.println(n);
        })
            .map(f -> f.intValue() == 200);

    }

    private Observable<Boolean> doTestResourceIsGone(String namespace, String resourceType, String name) {
        AtomicInteger backoffTime = new AtomicInteger(500);
        String url = createUrlBuilder().namespace(namespace).path(resourceType).path(name).build();

        return Observable.<Boolean> create(observer -> {
            m_httpClient.getStatus(url, new Subscriber<Integer>() {
                @Override
                public void onCompleted() {
                    observer.onCompleted();
                }

                @Override
                public void onError(Throwable t) {
                    observer.onError(t);
                }

                @Override
                public void onNext(Integer val) {
                    if (val == 404) {
                        observer.onNext(Boolean.TRUE);
                        observer.onCompleted();
                    }
                    else {
                        sleep(backoffTime.getAndUpdate(v -> v * 2));
                        observer.onError(new RuntimeException());
                    }
                }
            });
        }).retry(10);
    }

    private UrlBuilder createUrlBuilder() {
        Config cfg = m_config;
        return new UrlBuilder(cfg.getKubernetesURL(), cfg.getApiVersion());
    }

}
