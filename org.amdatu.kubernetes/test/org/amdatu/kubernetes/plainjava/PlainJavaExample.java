/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes.plainjava;

import io.fabric8.kubernetes.api.model.NodeList;

import org.amdatu.kubernetes.Kubernetes;
import org.amdatu.kubernetes.rest.KubernetesRestClient;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import rx.observers.TestSubscriber;

public class PlainJavaExample {
    private static final String KUBERNETES_URL = System.getenv("KUBERNETES_URL");
    private Kubernetes m_kubernetes;

    @Before
    public void setup() {
        Assume.assumeNotNull(KUBERNETES_URL);

        m_kubernetes = new KubernetesRestClient(KUBERNETES_URL, "v1");
    }

    @Test
    public void listNodes() {
        TestSubscriber<NodeList> testSubscriber = new TestSubscriber<>();
        m_kubernetes.listNodes().subscribe(testSubscriber);

        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
    }
}
