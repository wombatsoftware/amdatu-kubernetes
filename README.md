# Introduction

Amdatu Kubernetes is a client library for Kubernetes. It makes the Kubernetes REST API available to Java. This library is designed specifically to work well with OSGi, but can be easily used in any other Java environment as well.

# Usage within OSGi

In an OSGi application a service is registered with interface ```Kubernetes```. The service needs to be configured with the url to your Kuberentes API server using Configuration Admin. The PID of the service is ```org.amdatu.kubernetes```.

Using Felix Dependency Manager you can easily inject the Kubernetes service.




```
#!java

@Component
public class Example {
	@ServiceDependency	
	private volatile Kubernetes m_kubernetes;

	public void test() {
		kubernetes.listNodes().subscribe(nodes -> {
			nodes.getItems().forEach(System.out::println);
		});
	}
}
```

# Usage outside of OSGi

In a non-OSGi Java application you pass the Kubernetes API url to the constructor of the KubernetesRestClient.

```
#!java

public class PlainJavaExample {
	private Kubernetes m_kubernetes;
	
	@Before
	public void setup() {
		m_kubernetes = new KubernetesRestClient("http://[kubernetes-api]:8080", "v1");
	}
	
	@Test
	public void listNodes() {
		TestSubscriber<NodeList> testSubscriber = new TestSubscriber<>();
		m_kubernetes.listNodes().subscribe(testSubscriber);

		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
	}
}
```

For Gradle/Maven users the library is available on Bintray. 
The following Gradle setup contains the correct repository and coordinates.

```
#!java

repositories {
    maven {
        url  "http://dl.bintray.com/cloud-rti/maven"
    }

    mavenCentral()
}


dependencies {
    compile "org.amdatu:amdatu-kubernetes:1.0.4"
    compile "io.reactivex:rxjava:1.1.5"

}
```

# Using RX Observables

The Amdatu Kubernetes API is based on [RX](https://github.com/ReactiveX/RxJava), and returns Observables from all methods. This makes it much easier to deal with the asynchronous nature of a HTTP based API. 

Of course take a look at the [JavaDocs](https://amdatu.atlassian.net/builds/browse/KUBER-MAIN/latest/artifact/JOB1/javadoc/index.html).

The [integration tests](https://bitbucket.org/amdatulabs/amdatu-kubernetes/src/6850e7850f171ded7a814473e21128078791d929/org.amdatu.kubernetes.test/src/org/amdatu/kubernetes/test/KubernetesTest.java?at=master) of the project are the best place to learn how to use the API.